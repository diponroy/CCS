﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ccs.Web.ControllersApi
{
    public class HelloController : ApiController
    {
        [HttpGet]
        public string Wellcome()
        {
            return "Wellcome";
        }
    }
}
