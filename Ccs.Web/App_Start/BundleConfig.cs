﻿using System.Web.Optimization;

namespace Ccs.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/cores").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.css",
                "~/Content/metisMenu.css",
                "~/Content/App/sb-admin-2-theme.css"));

            bundles.Add(new ScriptBundle("~/Scripts/cores").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/metisMenu.js",
                "~/Scripts/App/sb-admin-2-theme.js"));
        }
    }
}