﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Ccs.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api",
                defaults: new { controller = "Hello", action = "Wellcome" }
            );
        }
    }
}
